const tabTitles = document.querySelectorAll('.tabs-title');

const tabContents = document.querySelectorAll('.tabs-content li');

tabTitles.forEach((tabTitle, index) => {
  tabTitle.addEventListener('click', () => {

    tabContents.forEach((content) => content.style.display = 'none');

    tabContents[index].style.display = 'block';

    tabTitles.forEach((tab) => tab.classList.remove('active'));
    tabTitle.classList.add('active');
  });
});
